package by.cnad.company.repo;

import by.cnad.company.entity.Company;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface CompanyRepository extends CrudRepository<Company, Long> {

  Optional<Company> findById(Long id);
}
